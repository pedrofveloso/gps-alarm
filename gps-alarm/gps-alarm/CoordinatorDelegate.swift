//
//  AppCoordinator.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol CoordinatorDelegate: AnyObject {
    var rootViewController: UIViewController? { get set }
    var navigation: UINavigationController? { get set }
    init(navigation: UINavigationController?)
    func start()
}
