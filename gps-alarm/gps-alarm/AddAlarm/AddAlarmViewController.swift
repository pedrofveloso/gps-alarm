//
//  AddAlarmViewController.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit
import MapKit

protocol AddAlarmViewDelegate: AnyObject {
    
}

class AddAlarmViewController: UIViewController {

    var presenter: AddAlarmPresenterDelegate? {
        didSet {
            self.presenter?.set(view: self)
        }
    }
    
    var resultSearchController: UISearchController?
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.showsUserLocation = true
        }
    }
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            self.addButton.setTitle("Adicionar alarme neste ponto", for: .normal)
        }
    }
    
    @IBAction func add() {
        // save
        presenter?.close()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
        presenter?.viewDidLoad()
    }
    
    private func setupSearchController() {
        let resultViewController = UIViewController.load(with: ResultsTableViewController.self)
        resultViewController.presenter = ResultsTablePresenter()
        self.resultSearchController = UISearchController(searchResultsController: resultViewController)
        self.resultSearchController?.searchResultsUpdater = resultViewController
        self.resultSearchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        guard let searchBar = self.resultSearchController?.searchBar else { return }
        searchBar.sizeToFit()
        self.navigationItem.titleView = searchBar
    }
}

extension AddAlarmViewController: MKMapViewDelegate {
    
}

extension AddAlarmViewController: AddAlarmViewDelegate {
    
}

extension AddAlarmViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchBarTextDidChange(to: searchText)
    }
}
