//
//  AddAlarmPresenter.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import CoreLocation

protocol AddAlarmPresenterDelegate: AnyObject {
    func set(view: AddAlarmViewDelegate)
    func close()
    func viewDidLoad()
    func searchBarTextDidChange(to text: String)
}

class AddAlarmPresenter {
    var coordinator: AddAlarmCoordinatorDelegate?
    private weak var view: AddAlarmViewDelegate?
    
    let locationManager = CLLocationManager()
    
    init(coordinator: AddAlarmCoordinatorDelegate) {
        self.coordinator = coordinator
    }
}

extension AddAlarmPresenter: AddAlarmPresenterDelegate {
    func searchBarTextDidChange(to text: String) {
        print("\(text)")
    }
    
    func close() {
        coordinator?.close()
    }
    
    func set(view: AddAlarmViewDelegate) {
        self.view = view
    }
    
    func viewDidLoad() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
}
