//
//  AddAlarmCoordinator.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol AddAlarmCoordinatorDelegate: AnyObject {
    func close()
}

class AddAlarmCoordinator {
    var rootViewController: UIViewController?
    var navigation: UINavigationController?
    
    required init(navigation: UINavigationController?) {
        guard let navigation = navigation else {
            self.navigation = UINavigationController(rootViewController: createAddAlarmViewController())
            return
        }
        self.navigation = navigation
    }
    
    private func createAddAlarmViewController() -> AddAlarmViewController{
        let vc = UIViewController.load(with: AddAlarmViewController.self)
        vc.presenter = AddAlarmPresenter(coordinator: self)
        return vc
    }
}

extension AddAlarmCoordinator: CoordinatorDelegate {
    func start() {
        self.navigation?.pushViewController(createAddAlarmViewController(), animated: true)
    }
}

extension AddAlarmCoordinator: AddAlarmCoordinatorDelegate {
    func close() {
        self.navigation?.popViewController(animated: true)
    }
}

