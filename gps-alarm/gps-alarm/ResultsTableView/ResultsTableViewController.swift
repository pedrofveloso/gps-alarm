//
//  ResultsTableViewController.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol ResultTableViewDelegate: AnyObject {
    
}

class ResultsTableViewController: UITableViewController {
    var presenter: ResultsTablePresenterDelegate? {
        didSet {
            self.presenter?.set(view: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

extension ResultsTableViewController: ResultTableViewDelegate {
    
}

extension ResultsTableViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}
