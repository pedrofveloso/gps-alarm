//
//  ResultsTablePresenter.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

protocol ResultsTablePresenterDelegate: AnyObject {
    func set(view: ResultTableViewDelegate)
}

class ResultsTablePresenter {
    private weak var view: ResultTableViewDelegate?
}

extension ResultsTablePresenter: ResultsTablePresenterDelegate {
    func set(view: ResultTableViewDelegate) {
        self.view = view
    }
}
