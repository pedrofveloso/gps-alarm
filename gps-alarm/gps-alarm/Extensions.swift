//
//  Extensions.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

extension UIViewController {
    class func load<T: UIViewController>(with: T.Type) -> T {
        return T.init(nibName: "\(T.self)", bundle: nil)
    }
}
