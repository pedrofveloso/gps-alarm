//
//  MainCoordinator.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class MainCoordinator {
    var rootViewController: UIViewController?
    var navigation: UINavigationController?
    
    required init(navigation: UINavigationController?) {
        guard let navigation = navigation else {
            self.navigation = UINavigationController(rootViewController: createViewController())
            return
        }
        self.navigation = navigation
    }
    
    private func createViewController() -> MainViewController {
        let vc = MainViewController()
        vc.presenter = MainPresenter(coordinator: self)
        return vc
    }
}

extension MainCoordinator: CoordinatorDelegate {
    func start() {
        navigation?.pushViewController(createViewController(), animated: true)
    }
}
