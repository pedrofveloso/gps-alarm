//
//  MainViewController.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol MainViewDelegate: AnyObject {
}

class MainViewController: UIViewController {
    //MARK: - Properties
    var presenter: MainPresenterDelegate? {
        didSet {
            self.presenter?.set(view: self)
        }
    }

    //MARK: - IBOutlets
    @IBOutlet weak var addAlarmButton: UIButton! {
        didSet {
            self.addAlarmButton.setTitle("Adicionar alarme", for: .normal)
        }
    }
    
    //MARK: - IBActions
    @IBAction func addAlarm() {
        presenter?.addAlarm()
    }
    
    
    //MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "GPS - Alarme"
        presenter?.viewDidLoad()

    }
}

extension MainViewController: MainViewDelegate {

}
