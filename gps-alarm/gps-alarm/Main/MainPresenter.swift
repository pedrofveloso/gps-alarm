//
//  MainPresenter.swift
//  gps-alarm
//
//  Created by resource on 02/07/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

protocol MainPresenterDelegate: AnyObject {
    func set(view: MainViewDelegate)
    func addAlarm()
    func viewDidLoad()
}

class MainPresenter {
    private weak var view: MainViewDelegate?
    private var coordinator: CoordinatorDelegate?
    
    init(coordinator: CoordinatorDelegate) {
        self.coordinator = coordinator
    }
}

extension MainPresenter: MainPresenterDelegate {
    func set(view: MainViewDelegate) {
        self.view = view
    }
    
    func addAlarm() {
        guard let navigation = coordinator?.navigation else { return }
        let coordinator = AddAlarmCoordinator(navigation: navigation)
        coordinator.start()
    }
    
    func viewDidLoad() {
        // load all alarms
    }
}
